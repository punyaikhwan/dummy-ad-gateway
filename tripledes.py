#! /usr/bin/python
import sys
from Crypto.Cipher import DES3
from Crypto.Util.Padding import pad

key = "123456789013245678901234"
# key = DES3.adjust_key_parity(key.encode('utf-8'))
cipher = DES3.new(key, DES3.MODE_ECB)

if len(sys.argv)!=2:
    print("No options passed.")
    exit()
password = sys.argv[1]
lendif = 8 - (len(password) % 8)

if (lendif == 8):
    lendif = 0
password = password.encode('utf-8')
password += ("\0"*lendif).encode('utf-8')
print(password)
result = cipher.encrypt(password)
print(result.hex())
print(cipher.decrypt(result))