from flask import Flask, request, jsonify
from Crypto.Cipher import DES3
from Crypto.Util.Padding import pad

app = Flask(__name__)

clientID = 'jkhgdadnfakfdsvcdnsjfsjsjdksyhdj'
hashCode = 'voiceid-prosa'
applicationID = 'voiceid-dashboard'

registeredUsers = {
    'admin' : 'prosa123456',
    'customerservicehalo' : 'customerservicehalo123',
    'helpdeskhalo' : 'helpdeskhalo123',
    'qualityassurance' : 'qualityassurance123',
    'securityadminhalo' : 'securityadminhalo123',
    'supervisorcshalo' : 'supervisorcshalo123'
}

key = "123456789013245678901234"
cipher = DES3.new(key, DES3.MODE_ECB)

@app.route('/ad-gateways/verify1', methods=['POST'])
def verify():
    if request.method == 'POST':
        try:
            print(request.headers)
            print(request.form)
            reqClientID = request.headers['ClientID']
            if 'HashCode' in request.headers.keys():
                reqHashCode = request.headers['HashCode']
            else:
                reqHashCode = hashCode
            reqApplicationID = request.form['ApplicationID']
            reqUserID = request.form['UserID']
            reqPassword = request.form['Password']

            if (reqClientID == clientID
                and reqHashCode == hashCode
                and reqApplicationID == applicationID):
                if reqUserID in registeredUsers.keys():
                    password = registeredUsers[reqUserID]
                    lendiff = 8 - (len(password) % 8)
                    if lendiff == 8:
                        lendiff = 0
                    password = password.encode('utf-8')
                    password += ("\0"*lendiff).encode('utf-8') #padding
                    password = cipher.encrypt(password)
                    password = password.hex()
                    if reqPassword == password:
                        strResponse = {
                            "ErrorSchema":    {
                                "ErrorCode": "ESB-00-000",
                                "ErrorMessage":       {
                                    "Indonesian": "Berhasil",
                                    "English": "Success"
                                }
                            },
                            "OutputSchema": {"Status": "0"}
                        }
                    else:
                        strResponse = {
                            "ErrorSchema":    {
                                "ErrorCode": "ESB-18-286",
                                "ErrorMessage":       {
                                    "Indonesian": "UserID atau Password salah",
                                    "English": "Wrong UserID or Password"
                                }
                            },
                            "OutputSchema": {"Status": "-1"}
                        }
                else:
                    strResponse = {
                        "ErrorSchema":    {
                            "ErrorCode": "ESB-18-286",
                            "ErrorMessage":       {
                                "Indonesian": "UserID atau Password salah",
                                "English": "Wrong UserID or Password"
                            }
                        },
                        "OutputSchema": {"Status": "-1"}
                    }
            else:
                strResponse = {
                    "ErrorSchema":    {
                        "ErrorCode": "ESB-18-288",
                        "ErrorMessage":       {
                            "Indonesian": "Value Attribute salah",
                            "English": "Wrong Value Attribute"
                        }
                    },
                    "OutputSchema": {"Status": "-3"}
                }
        except Exception as e:
            print(e.__str__)
            strResponse = {
                "ErrorSchema":    {
                    "ErrorCode": "ESB-18-287",
                    "ErrorMessage":       {
                        "Indonesian": "Key Attribute salah",
                        "English": "Wrong Key Attribute"
                    }
                },
                "OutputSchema": {"Status": "-2"}
            }
        finally:
            return jsonify(strResponse)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=9821)
